import Warrior from "./Warrior.js";
import WarriorAxe from "./WarriorAxe.js";
import WarriorSpear from "./WarriorSpear.js";
import WarriorSword from "./WarriorSword.js";

/*PART 1 : Warriors*/

let joan = new Warrior("Joan", 10, 10);
let leon = new Warrior("Leon", 15, 45);

joan.attack(leon); // leon perd 10 vies
leon.isAlive(); //retourne true

leon.attack(joan); //joan perds 15 vies
joan.isAlive(); //retourne false

/*PART 2 : Weapons */

let garfield = new WarriorAxe("Garfield", 20, 50);
let paula = new WarriorSword("Paula", 15, 50);
let varek = new WarriorSpear("Varek", 10, 50);

garfield.attack(varek);
varek.attack(paula); //classical attack
garfield.attack(paula); //powerful attack
paula.attack(varek); //powerful attack
varek.attack(garfield); //powerful attack

/*PART 3 : Battle */

function startBattle(warrior1, warrior2) {

  let result = "";

  console.log("New Battle !");
  console.log(
    `Our warriors are ${warrior1.name} (${warrior1.life} lives, equipped with ${warrior1.weapon.name} (${warrior1.power} damages)) and ${warrior2.name} (${warrior2.life} lives, equipped with ${warrior2.weapon.name} (${warrior2.power} damages))`
  );

  //attaques simultanées
  do {
    warrior1.attack(warrior2);
    warrior2.attack(warrior1);

    if (warrior1.life <= 0 && warrior2.life <= 0) {
      result = "It's a draw !"
    } else if (warrior1.life <= 0) {
      result = `${warrior2.name} wins !`;
    } else if (warrior2.life <= 0) {
      result = `${warrior1.name} wins !`;
    }
  } while (warrior1.life > 0 && warrior2.life > 0);

  console.log(result);
}

//création de nouvelles instances
let garen = new WarriorSword("Garen", 50, 200);
let darius = new WarriorAxe("Darius", 15, 80);

//lancer la battle entre les deux persos
startBattle(garen, darius);

