import Warrior from "./Warrior.js";
import WarriorSword from "./WarriorSword.js";
import Weapon from "./Weapon.js";

//hérite des propriétés et méthodes de la classe Warrior
class WarriorAxe extends Warrior{

    //constructeur de la classe WarriorAxe (reprend les mêmes paramètres que Warrior)
    constructor(name, power, life) {
        //appelle le constructeur de la classe Warrior avec les paramètres donnés
        super(name, power, life);
        //initialise la propriété weapon avec une nouvelle instance de la classe Weapon
        this.weapon = new Weapon("axe");
    }

    attack(opponent) {

        //vérifie si l'opposant est une instance de la classe WarriorSword
        if (opponent instanceof WarriorSword) {

         //multiplie la puissance par 2
            this.power *= 2;
            
          console.log(this.name + " used a powerful attack against " + opponent.name + "!");
            
          // Appelle la méthode attack de la classe parente Warrior
            super.attack(opponent);
            
          //remet la valeur de power à sa valeur initiale
            this.power /= 2;
            
        } else {
          // Appelle la méthode attack de la classe parente Warrior
          super.attack(opponent);
        }
        
    }

}

export default WarriorAxe;