class Warrior {
    weapon; //propriété qui ne nécessite pas d'initialisation spécifique à la création de l'objet
 
    //propriétés communes à toutes les instances de la classe, devant être initialisées à chaque création d'objets.
    constructor(name, power, life) {
        this.name = name;
        this.power = power;
        this.life = life;
    }

    attack(opponent) {
        opponent.life -= this.power;
        if (opponent.life <= 0) {
            console.log(this.name + " attacked " + opponent.name + ". " + opponent.name + " has 0 life left.");
        } else {
            console.log(this.name + " attacked " + opponent.name + ". " + opponent.name + " has " + opponent.life + " lives left.");
        }
    }

    isAlive() {
        console.log("Is "+ this.name + " still alive ?");
        console.log(this.life > 0);
    }
}

export default Warrior;